package masecla.pas.decompiler;

import org.jd.core.v1.ClassFileToJavaSourceDecompiler;

public class DecompilerHelper {
	private String file;

	public DecompilerHelper(String file) {
		super();
		this.file = file;
	}

	public String decompile() throws Exception {
		ClassFileToJavaSourceDecompiler decompiler = new ClassFileToJavaSourceDecompiler();
		PASLoader ld = new PASLoader();
		PASPrinter pr = new PASPrinter();
		decompiler.decompile(ld, pr, file);
		String source = pr.toString();
		return source;
	}
}
