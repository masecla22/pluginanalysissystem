package masecla.pas.decompiler;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.jd.core.v1.api.loader.Loader;
import org.jd.core.v1.api.loader.LoaderException;

public class PASLoader implements Loader {
	@Override
	public byte[] load(String classFile) throws LoaderException {
		InputStream is = null;
		try {
			is = new FileInputStream(new File(classFile));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		if (is == null) {
			return null;
		} else {
			try (InputStream in = is; ByteArrayOutputStream out = new ByteArrayOutputStream()) {
				byte[] buffer = new byte[1024];
				int read = in.read(buffer);

				while (read > 0) {
					out.write(buffer, 0, read);
					read = in.read(buffer);
				}

				return out.toByteArray();
			} catch (IOException e) {
				throw new LoaderException(e);
			}
		}
	}

	@Override
	public boolean canLoad(String classFile) {
		return new File(classFile).exists() && classFile.endsWith(".class");
	}
}
